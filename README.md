verbatlas-report
================

The code in this report generates an HTML report of
[VerbAtlas](https://verbatlas.org) for the purposes of
[RRGparbank](https://rrgparbank.phil.hhu.de) annotation. It is much uglier than
the interface at https://verbatlas.org but includes some information not
included there that is important for guiding annotations, viz. frame-specific
role definitions and example sentences, both obtained from PropBank via the
VerbAtlas-PropBank mapping.

Dependencies
------------

[NLTK](https://nltk.org) needs to be installed. The VerbAtlas relese needs to
be downloaded, unzipped and placed into a `data` subdirectory of this
repository.

Usage
-----

To generate the HTML report, run:

    make

This places the report in a file `out/verbatlas-1.0.13.html`.
