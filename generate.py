#!/usr/bin/env python3


import collections
import itertools
import nltk
import os
import re
import sys


from html import escape as h
from nltk.corpus import propbank
from nltk.corpus import wordnet31


def read_va(name):
    with open(f'data/VerbAtlas-1.1.0/VerbAtlas-1.1.0/{name}.tsv') as f:
        next(f)
        for line in f:
            line = line.rstrip()
            yield line.split('\t')


def element_to_role_id(element):
    if element.attrib['n'] == 'm':
        if 'f' in element.attrib:
            return 'AM-' + element.attrib['f'].upper()
        else:
            return 'AM'
    else:
        return 'A' + element.attrib['n']


def noncanonical_name(lemma):
    synsets = wordnet31.synsets(lemma.name(), pos=wordnet31.VERB)
    for i, s in enumerate(synsets, start=1):
        if s == lemma.synset():
            return f'{lemma.name()}.v.{i:02}'


if __name__ == '__main__':
    # Make sure resources are available
    nltk.download('propbank')
    nltk.download('wordnet31')
    # Map VerbAtlas frame IDs to their names
    frame_name_map = {f: n for f, n, _, _, _, _ in read_va('VA_frame_info')}
    frame_description_map = {f: d for f, _, d, _, _, _ in read_va('VA_frame_info')}
    # Map VerbAtlas frame IDs to their arguments
    frame_roles_map = {k: roles for k, *roles in read_va('VA_frame_pas')}
    # Map VerbAtlas frame IDs to BabelNet sense IDs
    frame_bn_map = collections.defaultdict(list)
    for bnfid, vafid in read_va('VA_bn2va'):
        frame_bn_map[vafid].append(bnfid)
    # Map BabelNet sense IDs to WordNet senses
    bn_wn_map = dict(read_va('bn2wn'))
    # Map WordNet senses to their IDs
    wn_sense_map = dict(read_va('wn2sense'))
    # Map VerbAtlas frame IDs to WordNet synsets
    frame_synset_map = collections.defaultdict(set)
    lemma_count = 0
    synset_count = 0
    for vafid in frame_bn_map:
        for bnfid in frame_bn_map[vafid]:
            if bnfid not in bn_wn_map:
                continue
            wnsid = bn_wn_map[bnfid]
            if wnsid not in wn_sense_map:
                continue
            sense = wn_sense_map[wnsid]
            form, _ = sense.split('%')
            lemmas = wordnet31.lemmas(form)
            lemma_count += len(lemmas)
            for lemma in lemmas:
                if lemma.key() == sense:
                    frame_synset_map[vafid].add(lemma.synset())
        synset_count += len(frame_synset_map[vafid])
    print(f'{lemma_count} lemmas', file=sys.stderr)
    print(f'{synset_count} synsets', file=sys.stderr)
    # Map VerbAtlas frames and PropBank frames to each other
    vapbmap = collections.defaultdict(dict)
    pbvamap = collections.defaultdict(dict)
    for frame_mapping, *role_mappings in read_va('pb2va'):
        pbfid, vafid = frame_mapping.split('>')
        vapbmap[vafid][pbfid] = {}
        pbvamap[pbfid] = {}
        for role_mapping in role_mappings:
            pbaid, vaaid = role_mapping.split('>')
            vapbmap[vafid][pbfid][vaaid] = pbaid
            pbvamap[pbfid][pbaid] = vaaid
    # Map PropBank frames to descriptions and examples
    descriptions = collections.defaultdict(dict)
    examples = collections.defaultdict(list)
    for roleset in propbank.rolesets():
        roleset_id = roleset.attrib['id']
        for role in roleset.find('roles').findall('role'):
            role_id = element_to_role_id(role)
            role_descr = role.attrib['descr']
            descriptions[roleset_id][role_id] = role_descr
        for example in roleset.findall('example'):
            text = example.find('text').text
            text = re.sub(r'\s+', ' ', text)
            for rel in example.findall('rel'):
                if not rel.text:
                    continue
                rel = rel.text
                text = text.replace(rel, f'[{rel}]<sub>V</sub>')
            for arg in example.findall('arg'):
                if not arg.text:
                    continue
                role_id = element_to_role_id(arg)
                arg = arg.text
                arg = re.sub(r'^(?:.* -> +|\*trace(?:-\d+)?\*=)', '', arg)
                arg = re.sub(r'\s+', ' ', arg)
                vaaid = pbvamap[roleset_id].get(role_id)
                if vaaid:
                    role_id = vaaid
                    text = text.replace(arg, f'[{h(arg)}]<sub>{h(role_id)}</sub>')
            examples[roleset_id].append(text)
    # Generate report
    print('<!doctype html>')
    print('<title>VerbAtlas 1.1.0 Frames</title>')
    print('<style>')
    print('''body {
        font-family: sans;
        font-size: small;
    }

    table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
        text-align: left;
        padding: 2px;
    }''')
    print('</style>')
    print('<h1>VerbAtlas 1.1.0 Frames</h1>')
    print('<p><i>An overview of <a href="https://verbatlas.org/">VerbAtlas</a> frames, their <a href="https://wordnet.princeton.edu/">WordNet 3.1</a> senses and mappings to <a href="https://propbank.github.io/">PropBank</a> frames</i></p>')
    frame_ids = sorted(frame_name_map, key=lambda i: frame_name_map[i])
    for frame_id in frame_ids:
        frame_name = frame_name_map[frame_id]
        print('<p>&nbsp;</p>')
        print(f'<h2 id="{h(frame_name)}">{h(frame_name)}</h2>')
        print(f'<p><i>{h(frame_description_map[frame_id])}</i></p>')
        print(f'<h3>Synsets</h3>')
        def synset_string(synset):
            definition = h(synset.definition())
            examples = synset.examples()
            if examples:
                definition += ': '
                definition += '; '.join(f'<i>{h(x)}</i>' for x in examples)
            forms = [noncanonical_name(l) for l in synset.lemmas()]
            forms = [f'<b>{h(f)}</b>' for f in forms]
            return f'{", ".join(forms)} ({definition})'
        synsets_string = '; '.join(synset_string(s) for s in frame_synset_map[frame_id])
        print(f'<p class=synsets>{synsets_string}</p>')
        print(f'<h3>Roles and PropBank Mappings</h3>')
        print('<table>')
        print('<tr>')
        print(f'<th>{h(frame_name)}</th>')
        for role in frame_roles_map.get(frame_id, []):
            print(f'<th>{h(role)}</th>')
        print('<th>Examples</th>')
        print('</tr>')
        for pbfid in vapbmap[frame_id]:
            print('<tr>')
            print(f'<th>{h(pbfid)}</td>')
            for role in frame_roles_map.get(frame_id, []):
                pbaid = vapbmap[frame_id][pbfid].get(role, '')
                descr = descriptions[pbfid].get(pbaid, '')
                print(f'<td>{h(descr)}</td>')
            print('<td>')
            for example in examples[pbfid]:
                print(f'{example}<br>')
            print('</td>')
            print('</tr>')
        print('</table>')
    print('</html>')
